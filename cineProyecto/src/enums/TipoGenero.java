package enums;


/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */

public enum TipoGenero {

    AVENTURA("Aventura"),
    FICCION("Ficción"),
    FANTASIA("Fantasía"),
    OESTE("Oeste"),
    FAMILIAR("Familiar"),
    TERROR("Terror"),
    COMEDIA("Comedia"),
    POLICIACA("Policiaca"),
    ROMANCE("Romantica"),
    ;
     String valor;

    /**
     * Método al que le paso un parámetro
     * @param valor
     */
   TipoGenero(String valor) {
        this.valor = valor;
    }

    /**
     * Método que retorna un parámetro
     * @return valor
     */
    public String getValor() {
        return valor;
    }



}
