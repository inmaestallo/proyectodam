package enums;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */
public enum TipoTecnico {

    CAMARA("Operador de cámara"),
    ILUMINACION("Iluminación"),
    EFECTOSESPECIALES("Efectos especiales"),
    SONIDO("Técnico de sonido"),
    MICROFONISTA("Microfonista"),
    CLAQUETISTA("Claquetista"),
    DECORADOR("Decorador"),
    VESTUARIO("Diseño de vestuario"),
    PELUQUERO("Peluquero"),
    MAQUILLAJE("Maquillaje"),
    ;
    private String valor;

    /**
     * Método al que le paso un parámetro
     * @param valor
     */
    TipoTecnico(String valor) {
        this.valor = valor;
    }

    /**
     * Método que retorna un parámetro
     * @return valor
     */
    public String getValor() {
        return valor;
    }

}
