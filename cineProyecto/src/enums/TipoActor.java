package enums;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */
public enum TipoActor {

    PROTAGONISTA("Protagonista"),
    PRIMARIO("Primario"),
    SECUNDARIO("Secundario"),
    DEREPARTO("De reparto"),
    CAMEO("Cameo"),
    DOBLE("Doble"),
    ESPECIALISTA("Especialista"),
    FIGURACION("Figuración"),
    ;
    private String valor;

    /**
     * Método al que se le pasa un parámetro
     * @param valor
     */
    TipoActor(String valor) {
        this.valor = valor;
    }

    /**
     * Método que retorna un parámetro
     * @return
     */
    public String getValor() {
        return valor;
    }

}
