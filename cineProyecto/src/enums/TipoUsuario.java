package enums;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */
public enum TipoUsuario {

    ADMINISTRADOR("Administrador"),
    USUARIO("Usuario"),
    CONSULTOR("Consultor");


    private String valor;

    /**
     * Método al que se le pasa un parámetro
     * @param valor
     */
    TipoUsuario(String valor) {
        this.valor = valor;
    }

    /**
     * Método que retorna un parámetro
     * @return valor
     */
    public String getValor() {
        return valor;
    }

}
