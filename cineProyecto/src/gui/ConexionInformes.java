package gui;

import java.sql.*;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */
public class ConexionInformes {

    private Connection con=null;

    /**
     * Método que conecta con la base de datos
     * @return
     */
    public Connection conectar(){



        String url="jdbc:mysql://127.0.0.1:3307/cine?user=root&password=";

        try {
            con= DriverManager.getConnection(url);


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return con;
    }


}
