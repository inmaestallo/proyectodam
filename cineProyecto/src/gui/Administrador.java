package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */


public class Administrador extends JFrame{


    JPanel panel1;
    JTextField txtTipoGenero;
    JButton btnTipoGenero;
    JTextField txtTipoActor;
    JButton buttonTipoActor;
     JTextField txtTipoTecnico;
     JButton btntipoTecnico;
   JButton iralaAplicacionButton;
    JFrame frame;
    List<Genero> generos;
    List<Actor> actores;
    List<Tecnico> tecnicos;

      Vista vista;


    /**
     * Constructor al que no se le pasa ningún parámetro
     */

    public Administrador() {


        frame = new JFrame("Administrador");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(new Dimension(500,500));
        frame.setVisible(true);

         interfaz2();
        generos = new ArrayList<Genero>();
        actores = new ArrayList<Actor>();
        tecnicos = new ArrayList<Tecnico>();

        btnTipoGenero.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Genero genero = new Genero();
                genero.setNombreG(txtTipoGenero.getText());
                generos.add(genero);

                String text = generos.toString().replace("[", "").replace("]", "");

                vista.comboBoxPeliculaGénero.addItem(text);
                generos.clear();

                    //Para volcar el arrayList en el fichero y visualizarlo


                    PrintWriter datos = null;

                    try {
                        datos = new PrintWriter(new BufferedWriter(new FileWriter("fichero.txt", true)));
                        datos.println(text);
                        datos.close();

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }


                }

        });


        iralaAplicacionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Modelo modelo = new Modelo();
                vista = new Vista();
                vista.panel1.setVisible(true);
                vista.eliminarButton.setVisible(true);
                vista.eliminarTipoDePersonajeButton.setVisible(true);
                vista.eliminarTipoDeTécnicoButton.setVisible(true);
                Controlador controlador = new Controlador(modelo, vista);
            }
        });


        buttonTipoActor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                Actor actor = new Actor();
                actor.setNombreActor(txtTipoActor.getText());
                actores.add(actor);


                String text = actores.toString().replace("[", "").replace("]", "");

                vista.comboBoxTipoPersonaje.addItem(text);
                actores.clear();


                PrintWriter datos = null;

                try {
                    datos = new PrintWriter(new BufferedWriter(new FileWriter("ficheroActor.txt", true)));
                    datos.println(text);
                    datos.close();

                } catch (IOException ex) {
                    ex.printStackTrace();
                }


            }
        });

        btntipoTecnico.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Tecnico tecnico = new Tecnico();
                tecnico.setNombreTecnnico(txtTipoTecnico.getText());
                tecnicos.add(tecnico);
                String text = tecnicos.toString().replace("[", "").replace("]", "");
                vista.comboBoxTecnico.addItem(text);
                tecnicos.clear();


                PrintWriter datos = null;

                try {
                    datos = new PrintWriter(new BufferedWriter(new FileWriter("ficheroTecnico.txt", true)));


                    datos.println(text);
                    datos.close();

                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        });


    }

    public void interfaz2(){

        Image img = new ImageIcon("imagenes/añador2.jpeg").getImage();
        ImageIcon img2 = new ImageIcon(img.getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        btnTipoGenero.setIcon(img2);

        Image imgLupa = new ImageIcon("imagenes/añador2.jpeg").getImage();
        ImageIcon img3 = new ImageIcon(imgLupa.getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        buttonTipoActor.setIcon(img3);

        Image imgLupa4 = new ImageIcon("imagenes/añador2.jpeg").getImage();
        ImageIcon img5 = new ImageIcon(imgLupa4.getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        btntipoTecnico.setIcon(img5);

        Image imgLupa5 = new ImageIcon("imagenes/irApli.png").getImage();
        ImageIcon img6 = new ImageIcon(imgLupa5.getScaledInstance(50, 50, Image.SCALE_AREA_AVERAGING));
        iralaAplicacionButton.setIcon(img6);


    }
}

