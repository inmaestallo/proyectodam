package gui;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;



/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */

public class Informes extends JFrame{
    private JPanel panel1;
    JButton buttonInformePadre;
     JButton buttonInformeActores;
     JButton buttonInformeCantidad;
    private JPanel panelInforme;
    private JTabbedPane tabbedPane1;
     JLabel informes;
    private JLabel pelicula;
    private JLabel actores;
    private JLabel dinero;
    JFrame frame;
    Connection conexion;


    /**
     * Constructor al que no se le pasa ningún parámetro
     */

    public Informes() {


        frame = new JFrame("Consultor");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(new Dimension(500,500));
        frame.setVisible(true);
        this.conexion= new ConexionInformes().conectar();
        frame.setLocationRelativeTo(null);
        interfaz3();












        buttonInformePadre.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                JasperPrint jasperPrint;

                try {
                    Map<String,Object> parametros =new HashMap<String,Object>();

                    parametros.put("titulo","PELÍCULAS DE CADA PRODUCTORA");
                    parametros.put("autor", "Inma Estallo Puyuelo");
                    jasperPrint = JasperFillManager.fillReport("InformeCinePadre.jasper",parametros,conexion);
                    JRPdfExporter exp=new JRPdfExporter();
                    exp.setExporterInput(new SimpleExporterInput(jasperPrint));
                    exp.setExporterOutput(new SimpleOutputStreamExporterOutput("InformeCP.pdf"));
                    SimplePdfExporterConfiguration conf=new SimplePdfExporterConfiguration();
                    exp.exportReport();


                    JasperViewer jasperViewer=new JasperViewer(jasperPrint, false);
                    jasperViewer.setVisible(true);



                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });
        buttonInformeActores.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                JasperPrint jasperPrint;

                try {
                    Map<String,Object> parametros =new HashMap<String,Object>();

                    parametros.put("titulo","LISTA DE ACTORES");
                    parametros.put("autor", "Inma Estallo Puyuelo");
                    jasperPrint = JasperFillManager.fillReport("InformeCine1.jasper",parametros,conexion);
                    JRPdfExporter exp=new JRPdfExporter();
                    exp.setExporterInput(new SimpleExporterInput(jasperPrint));
                    exp.setExporterOutput(new SimpleOutputStreamExporterOutput("Informe1.pdf"));
                    SimplePdfExporterConfiguration conf=new SimplePdfExporterConfiguration();
                    exp.exportReport();


                    JasperViewer jasperViewer=new JasperViewer(jasperPrint, false);
                    jasperViewer.setVisible(true);



                } catch (JRException ex) {
                    ex.printStackTrace();
                }

            }
        });
        buttonInformeCantidad.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


               pintarInforme();
            }

        });
    }

    /**
     * Método con el que se pinta un informe
     */
    private void pintarInforme() {

        JasperPrint jasperPrint;

        try {
            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("titulo", "DINERO DE CADA PRODUCTORA");
            parametros.put("autor", "Inma Estallo Puyuelo");
            jasperPrint = JasperFillManager.fillReport("InformeCIneGrafico.jasper", parametros, conexion);
            JRPdfExporter exp = new JRPdfExporter();
            exp.setExporterInput(new SimpleExporterInput(jasperPrint));
            exp.setExporterOutput(new SimpleOutputStreamExporterOutput("informeGrafico.pdf"));
            SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
            exp.exportReport();


            JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
            jasperViewer.setVisible(true);
        } catch (JRException ex) {
            ex.printStackTrace();

        }
    }
    public void interfaz3(){

        Image img = new ImageIcon("imagenes/informe1.png").getImage();
        ImageIcon img2 = new ImageIcon(img.getScaledInstance(90, 90, Image.SCALE_AREA_AVERAGING));
        informes.setIcon(img2);

        Image imgLupa = new ImageIcon("imagenes/descarga (2).png").getImage();
        ImageIcon img3 = new ImageIcon(imgLupa.getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        buttonInformePadre.setIcon(img3);

        Image imgLupa4 = new ImageIcon("imagenes/actor.jpeg").getImage();
        ImageIcon img5 = new ImageIcon(imgLupa4.getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        buttonInformeActores.setIcon(img5);

        Image imgLupa6 = new ImageIcon("imagenes/tecnicos.jpeg").getImage();
        ImageIcon img7 = new ImageIcon(imgLupa6.getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        buttonInformeCantidad.setIcon(img7);




    }

}





