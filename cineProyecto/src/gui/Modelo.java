package gui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */

public class Modelo {

    private Connection conexion;

    /**
     * Método que conecta la base de datos
     * @return null;
     */
   public Connection conectar() {

        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/cine", "root", "");
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/cine", "root", "");

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
       return null;
   }

    /**
     * Método que desconecta la base de datos
     */
   public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    /**
     * Método que lee de fichero un sql
     * @return stringBuilder.toString();
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("cine_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    /**
     * Método que inserta una productora
     * @param nombreProductora
     * @param fundador
     * @param fechaCreacion
     * @param ciudad
     * @param pais
     * @param cantidadPersonas
     * @param cantidadDinero
     */
    public void insertarProductora(String nombreProductora,String fundador,LocalDate fechaCreacion,String ciudad, String pais, int cantidadPersonas, int cantidadDinero) {
        String sentenciaSql = "INSERT INTO  productora(nombreProductora,fundador, fechanCreacionP,ciudad,pais,cantidadPersonas,cantidadDinero) VALUES (?, ?, ?, ? , ? , ? , ? )";
        PreparedStatement sentencia = null;


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombreProductora);
            sentencia.setString(2,fundador);
            sentencia.setDate(3, Date.valueOf(fechaCreacion));
            sentencia.setString(4,ciudad);
            sentencia.setString(5,pais);
            sentencia.setInt(6, cantidadPersonas);
            sentencia.setInt(7, cantidadDinero);
            sentencia.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que modifica una productora
     * @param nombreProductora
     * @param fundador
     * @param fechaCreacion
     * @param ciudad
     * @param pais
     * @param cantidadPersonas
     * @param cantidadDinero
     * @param idProductora
     */
   public void modificarProductora(String nombreProductora,String fundador,LocalDate fechaCreacion,String ciudad, String pais, int cantidadPersonas, int cantidadDinero, int idProductora) {

        String sentenciaSql = "UPDATE productora SET nombreProductora = ?,fundador= ?, fechanCreacionP = ?, ciudad= ?, pais= ?, cantidadPersonas= ?, cantidadDinero= ? WHERE idProductora = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombreProductora);
            sentencia.setString(2,fundador);
            sentencia.setDate(3, Date.valueOf(fechaCreacion));
            sentencia.setString(4,ciudad);
            sentencia.setString(5,pais);
            sentencia.setInt(6, cantidadPersonas);
            sentencia.setInt(7, cantidadDinero);
            sentencia.setInt(8, idProductora);

            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que borra una productora
     * @param idProductora
     */
    public void borrarProductora(int idProductora) {
        String sentenciaSql = "DELETE FROM productora WHERE idProductora = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idProductora);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que consulta un productora
     * @return resultado
     * @throws SQLException
     */
    ResultSet consultarProductora() throws SQLException {
        String sentenciaSql = "SELECT concat(idProductora) as 'ID',concat(nombreProductora)as 'nombre',concat(fundador)as 'Fundador'," +
                "concat(fechanCreacionP) as 'fecha Creacion', concat(ciudad) as 'ciudad', concat(pais) as 'pais'" +
                ", concat(cantidadPersonas) as 'cantidad de personas', concat(cantidadDinero) as 'cantidad de dinero' FROM productora as productoras " ;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método que busca un productora
     * @param valor
     * @return resultado
     * @throws SQLException
     */

    public ResultSet buscarProductora(String valor) throws SQLException {


        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT * FROM productora WHERE CONCAT(idProductora ,nombreProductora,fundador, fechanCreacionP,ciudad,pais,cantidadPersonas,cantidadDinero ) LIKE'%" +valor+"%'";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery( consulta);




        return resultado;

    }

    /**
     * Método que inserta una película
     * @param nombrePelicula
     * @param directorPelicula
     * @param fechaInicioGra
     * @param fechaFinnalGra
     * @param idProductora
     * @param tipoGenero
     * @param lugarGrabacion
     */
    public void insertarPelicula(String nombrePelicula,String directorPelicula,LocalDate fechaInicioGra,LocalDate fechaFinnalGra ,String idProductora,String tipoGenero, String lugarGrabacion) {
        String sentenciaSql = "INSERT INTO  pelicula(nombrePelicula, directorPelicula, fechanInicioGraba,fechanFinGraba,idProductora,genero,lugarGrabacion) VALUES (?, ?, ?, ? , ? ,? , ?)";
        PreparedStatement sentencia = null;
        int idproductora = Integer.valueOf(idProductora.split(" ")[0]);


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombrePelicula);
            sentencia.setString(2,directorPelicula);
            sentencia.setDate(3, Date.valueOf(fechaInicioGra));
            sentencia.setDate(4, Date.valueOf(fechaFinnalGra));
            sentencia.setInt(5, idproductora);
            sentencia.setString(6, tipoGenero);
            sentencia.setString(7,lugarGrabacion);
            sentencia.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que modifica una película
     * @param nombrePelicula
     * @param directorPelicula
     * @param fechaInicioGra
     * @param fechaFinnalGra
     * @param idProductora
     * @param tipoGenero
     * @param lugarGrabacion
     * @param idPelicula
     */
   public void modificarPelicula(String nombrePelicula,String directorPelicula,LocalDate fechaInicioGra,LocalDate fechaFinnalGra ,String idProductora,String tipoGenero, String lugarGrabacion, int idPelicula) {

        String sentenciaSql = "UPDATE pelicula SET nombrePelicula = ?, directorPelicula = ?, fechanInicioGraba= ?, fechanFinGraba= ?, idProductora= ?, genero= ?, lugarGrabacion= ? WHERE idPelicula = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombrePelicula);
            sentencia.setString(2,directorPelicula);
            sentencia.setDate(3, Date.valueOf(fechaInicioGra));
            sentencia.setDate(4, Date.valueOf(fechaFinnalGra));
            sentencia.setInt(5, Integer.parseInt(idProductora));
            sentencia.setString(6, tipoGenero);
            sentencia.setString(7,lugarGrabacion);
            sentencia.setInt(8, idPelicula);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método qie borra una película
     * @param idPelicula
     */
    public void borrarPelicula(int idPelicula) {
        String sentenciaSql = "DELETE FROM pelicula WHERE idPelicula = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idPelicula);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método para consultar un película
     * @return resultado
     * @throws SQLException
     */
    ResultSet consultarPelicula() throws SQLException {
        String sentenciaSql = "SELECT concat(idPelicula) as 'ID',concat(nombrePelicula)as 'nombre',concat(directorPelicula)as 'director'," +
                "concat(fechanInicioGraba) as 'fecha inicio de grabacion', concat(fechanFinGraba) as 'fecha fin grabacion', concat(peliculas.idProductora) as 'id Productora'" +
                ", concat(genero) as 'Genero', concat(lugarGrabacion) as 'Lugar de grabacion' FROM pelicula as peliculas " +
                "inner join productora as productoras on productoras.idProductora = peliculas.idProductora "  ;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método para buscar una película
     * @param valor
     * @return resultado
     * @throws SQLException
     */
    public ResultSet buscarPelicula(String valor) throws SQLException {


        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT * FROM pelicula WHERE CONCAT(idPelicula ,nombrePelicula, directorPelicula, fechanInicioGraba,fechanFinGraba,idProductora,genero,lugarGrabacion) LIKE'%" +valor+"%'";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery( consulta);




        return resultado;

    }

    /**
     * Método para insertar un actor
     * @param nombreActor
     * @param apellidoActor
     * @param edadActor
     * @param tipoPersonaje
     * @param nacionalidad
     * @param premio
     * @param experiencia
     * @param idPelicula
     */
    public void insertarActor(String nombreActor,String apellidoActor,int edadActor,String tipoPersonaje,String nacionalidad ,int premio, int experiencia,String idPelicula) {
        String sentenciaSql = "INSERT INTO  actor(nombreActor, apellidoActor, edadActor,tipoPersonaje,nacionalidad,premios,experienciaActor,idPelicula) VALUES (?, ?, ?, ? , ? ,? , ?,?)";
        PreparedStatement sentencia = null;
        int idpelicula = Integer.valueOf(idPelicula.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombreActor);
            sentencia.setString(2,apellidoActor);
            sentencia.setInt(3, edadActor);
            sentencia.setString(4,tipoPersonaje);
            sentencia.setString(5,nacionalidad);
            sentencia.setInt(6, premio);
            sentencia.setInt(7, experiencia);
            sentencia.setInt(8, idpelicula);
            sentencia.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método para modificar un actor
     * @param nombreActor
     * @param apellidoActor
     * @param edadActor
     * @param tipoPersonaje
     * @param nacionalidad
     * @param premio
     * @param experiencia
     * @param idPelicula
     * @param idactor
     */
    public void modificarActor(String nombreActor,String apellidoActor,int edadActor,String tipoPersonaje,String nacionalidad ,int premio, int experiencia,String idPelicula,int idactor) {

        String sentenciaSql = "UPDATE actor SET nombreActor = ?, apellidoActor = ?, edadActor= ?, tipoPersonaje= ?, nacionalidad= ?, premios= ?, experienciaActor= ?,idPelicula=? WHERE idActor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombreActor);
            sentencia.setString(2,apellidoActor);
            sentencia.setInt(3, edadActor);
            sentencia.setString(4,tipoPersonaje);
            sentencia.setString(5,nacionalidad);
            sentencia.setInt(6, premio);
            sentencia.setInt(7, experiencia);
            sentencia.setInt(8, Integer.parseInt(idPelicula));
            sentencia.setInt(9, idactor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método para borrar un actor
     * @param idActor
     */
    public void borrarActor(int idActor) {
        String sentenciaSql = "DELETE FROM actor WHERE idActor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idActor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método para consultar un actor
     * @return resultado
     * @throws SQLException
     */
    ResultSet consultarActor() throws SQLException {
        String sentenciaSql = "SELECT concat(idActor) as 'Id actor', concat(nombreActor) as" +
                " 'Nombre',concat(apellidoActor) as 'Apellido', concat(edadActor) as 'Edad', " +
                "concat(tipoPersonaje) as 'Tipo de personaje', concat(nacionalidad) as 'Nacionalidad'," +
                " concat(premios) as 'Premios', concat(experienciaActor) as 'Experiencia', concat(actores.idPelicula) as 'Id pelicula'" +
                " FROM actor as actores inner join pelicula as peliculas on peliculas.idPelicula = actores.idPelicula ";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método para buscar un actor
     * @param valor
     * @return resultado
     * @throws SQLException
     */
    public ResultSet buscarActor(String valor) throws SQLException {


        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT * FROM actor WHERE CONCAT(idActor ,nombreActor, apellidoActor, edadActor,tipoPersonaje,nacionalidad,premios,experienciaActor,idPelicula) LIKE'%" +valor+"%'";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery( consulta);




        return resultado;

    }

    /**
     * Métodopara insertar un técnico
     * @param nombreTecnico
     * @param apellidoTecnico
     * @param edadTecnico
     * @param tipoTecnico
     * @param aniosTrabajados
     * @param fechaInicioT
     * @param fechaFinalT
     * @param idPelicula
     */

    public void insertarTecnico(String nombreTecnico,String apellidoTecnico,int edadTecnico,String tipoTecnico,int aniosTrabajados, LocalDate fechaInicioT,LocalDate fechaFinalT,String idPelicula) {
        String sentenciaSql = "INSERT INTO  tecnico(nombreTecnico, apellidoTecnico, edadTecnico,tipoTecnico,aniosTrabajados,fechaInicioT,fechaFinalT,idPelicula) VALUES (?, ?, ?, ? , ? ,? , ?,?)";
        PreparedStatement sentencia = null;
        int idpelicula = Integer.valueOf(idPelicula.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombreTecnico);
            sentencia.setString(2,apellidoTecnico);
            sentencia.setInt(3, edadTecnico);
            sentencia.setString(4,tipoTecnico);
            sentencia.setInt(5, aniosTrabajados);
            sentencia.setDate(6, Date.valueOf(fechaInicioT));
            sentencia.setDate(7, Date.valueOf(fechaFinalT));
            sentencia.setInt(8, idpelicula);
            sentencia.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método para modificar un técnico
     * @param nombreTecnico
     * @param apellidoTecnico
     * @param edadTecnico
     * @param tipoTecnico
     * @param aniosTrabajados
     * @param fechaInicioT
     * @param fechaFinalT
     * @param idPelicula
     * @param idTecnico
     */
    public void modificarTecnico(String nombreTecnico,String apellidoTecnico,int edadTecnico,String tipoTecnico,int aniosTrabajados, LocalDate fechaInicioT,LocalDate fechaFinalT,String idPelicula,int idTecnico) {

        String sentenciaSql = "UPDATE tecnico SET nombreTecnico = ?, apellidoTecnico = ?, edadTecnico= ?, tipoTecnico= ?, aniosTrabajados= ?, fechaInicioT= ?, fechaFinalT= ?,idPelicula=? WHERE idTecnico = ?";
        PreparedStatement sentencia = null;



        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombreTecnico);
            sentencia.setString(2,apellidoTecnico);
            sentencia.setInt(3, edadTecnico);
            sentencia.setString(4,tipoTecnico);
            sentencia.setInt(5, aniosTrabajados);
            sentencia.setDate(6, Date.valueOf(fechaInicioT));
            sentencia.setDate(7, Date.valueOf(fechaFinalT));
            sentencia.setInt(8, Integer.parseInt(idPelicula));
            sentencia.setInt(9, idTecnico);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método para modificar un técnico
     * @param idTecnico
     */
    public void borrarTecnico(int idTecnico) {
        String sentenciaSql = "DELETE FROM tecnico WHERE idTecnico = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idTecnico);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método para buscar un técnico
     * @return resultado
     * @throws SQLException
     */
    ResultSet consultarTecnico() throws SQLException {
        String sentenciaSql = "SELECT concat(idTecnico) as 'Id actor', concat(nombreTecnico) as" +
                " 'Nombre',concat(apellidoTecnico) as 'Apellido', concat(edadTecnico) as 'Edad', " +
                "concat(tipoTecnico) as 'Tipo de tecnico', concat(aniosTrabajados) as 'Años trabajados'," +
                " concat(fechaInicioT) as 'Fecha de inicio de trabajo', concat(fechaFinalT) as 'Fecha final de trabajo' , concat(tecnicos.idPelicula) as 'id Pelicula'" +
                "FROM tecnico as tecnicos inner join pelicula as peliculas on peliculas.idPelicula = tecnicos.idPelicula ";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método para buscar un técnico
     * @param valor
     * @return
     * @throws SQLException
     */

    public ResultSet buscarTecnico(String valor) throws SQLException {


        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT * FROM tecnico WHERE CONCAT(idTecnico ,nombreTecnico, apellidoTecnico, edadTecnico,tipoTecnico,aniosTrabajados,fechaInicioT,fechaFinalT,idPelicula) LIKE'%" +valor+"%'";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery( consulta);




        return resultado;

    }

    /**
     * Método paea saber si existe la productora
     * @param nombre
     * @return productoraExists;
     */
    public boolean productoraYaExiste(String nombre) {
        String salesConsult = "SELECT existeProductora(?)";
        PreparedStatement function;
        boolean productoraExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1,nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            productoraExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productoraExists;
    }

    /**
     * Método que te dice si existe una película
     * @param nombre
     * @return peliculaExists;
     */
    public boolean peliculaYaExiste(String nombre) {
        String salesConsult = "SELECT existePelicula(?)";
        PreparedStatement function;
        boolean peliculaExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1,nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            peliculaExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return peliculaExists;
    }

    /**
     * Método que te dice si existe el actor
     * @param nombre
     * @return  actorExists;
     */
    public boolean actorYaExiste(String nombre) {
        String salesConsult = "SELECT existeActor(?)";
        PreparedStatement function;
        boolean actorExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1,nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            actorExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actorExists;
    }

    /**
     * Método que te dice si existe el técnico
     * @param nombre
     * @return tecnicoExists;
     */
    public boolean tecnicoYaExiste(String nombre) {
        String salesConsult = "SELECT existeTecnico(?)";
        PreparedStatement function;
        boolean tecnicoExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1,nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            tecnicoExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tecnicoExists;
    }
}
