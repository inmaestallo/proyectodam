package gui;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */
public class Actor {
    String nombreActor;

    /**
     * Constructor al que le paso un parámetro
     * @param nombreActor
     */
    public Actor(String nombreActor) {
        this.nombreActor = nombreActor;
    }

    /**
     * Constructor vacio
     */
    public Actor() {
    }

    /**
     * Método que retorna el nombreActor
     * @return nombreActor
     */
    public String getNombreActor() {
        return nombreActor;
    }

    /**
     * Método al que se le pasa un parámetro
     * @param nombreActor
     */
    public void setNombreActor(String nombreActor) {
        this.nombreActor = nombreActor;
    }

    /**
     * Método que visualiza el nomberActor
     * @return
     */
    @Override
    public String toString() {
        return  nombreActor ;
    }
}
