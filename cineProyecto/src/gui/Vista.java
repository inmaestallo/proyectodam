package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.TipoActor;
import enums.TipoGenero;
import enums.TipoTecnico;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Vista extends JFrame {
    private JTabbedPane tabbedPane;
    JPanel panel1;
    JFrame frame1;

    JTextField txtNombreProductora;
    JTextField txtCiudad;
    JTextField txtpaisProductora;
    JTextField txtCantidadPersonasPR;
    JTextField txtNombrePelicula;
    JTextField txtBuscarProductora;
    JTextField txtDirector;
    JTextField txtLugarGraba;
    JTextField txtBuscarPelicula;
    JTextField txtCantidadDinero;
    JTextField txtNombreActor;
    JTextField txtApellidoActor;
    JTextField txtEdadActor;
    JTextField txtNacionalidad;
    JTextField txtPremios;
    JTextField txtExperiencia;
    JTextField txtBuscarActor;
    JTextField txtNombeTecnico;
    JTextField txtApellido;
    JTextField txtEdadTecnico;
    JTextField textFieldAniosTecnico;
    JTextField txtBuscarTecnico;
    JTextField txtFundador;

    DatePicker fechaProductora;
    DatePicker FechainicioTecnico;
    DatePicker fechaFinalTecnico;
    DatePicker fechaInicioPelicula;
    DatePicker fechaFinPelicula;

    JButton añadirProductoraButton;
    JButton eliminarProductoraButton;
    JButton añadirPeliculaButton1;
    JButton eliminarPeliculaButton1;
    JButton añadirActorButton;
    JButton eliminarActorButton;
    JButton buttonAnadirTecnico;
    JButton eliminarTecnicoButton2;

    JComboBox comboBoxProductora;
    JComboBox comboBoxPeliculaProductora;
    JComboBox comboBoxPeliculaGénero;
    JComboBox comboBoxTipoPersonaje;
    JComboBox comboBoxTecnico;
    JComboBox comboBox1PeliculaActor;
    JComboBox comboBoxTecnicoPelicula;

    DefaultTableModel dtmProductora;
    DefaultTableModel dtmPelicula;
    DefaultTableModel dtmActor;
    DefaultTableModel dtmTecnico;

    JTable tableProductora;
    JTable tablePelicula;
    JTable tableActor;
    JTable tableTecnico;

    JLabel lblAccion;
    JLabel lblAccionActor;
    JLabel lblAccionPeli;
    JLabel lblAccionTecnico;
    JLabel buscarProdu;
    private JLabel buscarPeli;
    private JLabel buscarActor;
    private JLabel buscarTecni;
    JButton eliminarButton;
    JButton eliminarTipoDePersonajeButton;
    JButton eliminarTipoDeTécnicoButton;

    JLabel letraTipoGenero;
    Administrador administrador;
    private DefaultComboBoxModel<Genero> dcbm;



    /* MENUBAR */
    JMenuItem salirItem;
    JMenuItem conexionItem;

    private ImageIcon ImagenIcono;


    InicioSesion optionDialog;

    public Vista() {

        frame1 = new JFrame("Manager Cinema");
        frame1.setContentPane(panel1);
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame1.pack();
        frame1.setVisible(true);
        frame1.setLocationRelativeTo(null);
        eliminarButton.setVisible(false);
        eliminarTipoDePersonajeButton.setVisible(false);
        eliminarTipoDeTécnicoButton.setVisible(false);


        interfaz();
        setMenu();
        setEnumComboBox();
        setTableModels();


          fichero();
          ficheroActor();
          ficheroTecnico();


        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                    File fichero = new File("fichero.txt");
                    Scanner lector = null;
                    try {
                        lector = new Scanner(fichero);

                        while (lector.hasNextLine()) {
                            comboBoxPeliculaGénero.removeItem(lector.nextLine());
                        }
                        lector.close();

                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                    }



            }
        });
        eliminarTipoDePersonajeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                File fichero=new File("ficheroActor.txt");
                Scanner lector = null;
                try {
                    lector = new Scanner(fichero);

                    while(lector.hasNextLine()){
                        comboBoxTipoPersonaje.removeItem(lector.nextLine());
                    }
                    lector.close();

                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }

            }
        });
        eliminarTipoDeTécnicoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File fichero=new File("ficheroTecnico.txt");
                Scanner lector = null;
                try {
                    lector = new Scanner(fichero);

                    while(lector.hasNextLine()){
                        comboBoxTecnico.removeItem(lector.nextLine());
                    }
                    lector.close();

                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }


            }
        });
    }


    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    private void setTableModels() {
        this.dtmProductora = new DefaultTableModel();
        this.tableProductora.setModel(dtmProductora);

        this.dtmPelicula = new DefaultTableModel();
        this.tablePelicula.setModel(dtmPelicula);

        this.dtmActor = new DefaultTableModel();
        this.tableActor.setModel(dtmActor);

        this.dtmTecnico = new DefaultTableModel();
        this.tableTecnico.setModel(dtmTecnico);
    }


    public void setEnumComboBox() {

        for (TipoActor constant : TipoActor.values()) {
            comboBoxTipoPersonaje.addItem(constant.getValor());


        }
        comboBoxTipoPersonaje.setSelectedIndex(-1);

        for (TipoTecnico constant : TipoTecnico.values()) {
            comboBoxTecnico.addItem(constant.getValor());
        }
        comboBoxTecnico.setSelectedIndex(-1);

        for (TipoGenero constant : TipoGenero.values()) {
            comboBoxPeliculaGénero.addItem(constant.getValor());
        }
        comboBoxPeliculaGénero.setSelectedIndex(-1);

    }


    /**
     * Método en el que añadimos las imágenes a la interfaz
     */
    public void interfaz(){

        Image img= new ImageIcon("imagenes/lupa.jpeg").getImage();
        ImageIcon img2=new ImageIcon(img.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        buscarProdu.setIcon(img2);

        Image imgLupa= new ImageIcon("imagenes/lupa.jpeg").getImage();
        ImageIcon img3=new ImageIcon(imgLupa.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        buscarPeli.setIcon(img3);

        Image imgLupa4= new ImageIcon("imagenes/lupa.jpeg").getImage();
        ImageIcon img5=new ImageIcon(imgLupa4.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        buscarActor.setIcon(img5);

        Image imgLupa3= new ImageIcon("imagenes/lupa.jpeg").getImage();
        ImageIcon img4=new ImageIcon(imgLupa3.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        buscarTecni.setIcon(img4);

        Image imgAñadir= new ImageIcon("imagenes/añadir.png").getImage();
        ImageIcon añadir=new ImageIcon(imgAñadir.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        añadirProductoraButton.setIcon(añadir);

        Image imgAñadir2= new ImageIcon("imagenes/añadir.png").getImage();
        ImageIcon añadir2=new ImageIcon(imgAñadir2.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        añadirPeliculaButton1.setIcon(añadir2);

        Image imgAñadir3= new ImageIcon("imagenes/añadir.png").getImage();
        ImageIcon añadir3=new ImageIcon(imgAñadir3.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        añadirActorButton.setIcon(añadir3);

        Image imgAñadir4= new ImageIcon("imagenes/añadir.png").getImage();
        ImageIcon añadir4=new ImageIcon(imgAñadir4.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        buttonAnadirTecnico.setIcon(añadir4);


        Image imgEliminar= new ImageIcon("imagenes/papelera.jpeg").getImage();
        ImageIcon eliminar=new ImageIcon(imgEliminar.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        eliminarProductoraButton.setIcon(eliminar);

        Image imgEliminar2= new ImageIcon("imagenes/papelera.jpeg").getImage();
        ImageIcon eliminar2=new ImageIcon(imgEliminar2.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        eliminarPeliculaButton1.setIcon(eliminar2);

        Image imgEliminar3= new ImageIcon("imagenes/papelera.jpeg").getImage();
        ImageIcon eliminar3=new ImageIcon(imgEliminar3.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        eliminarActorButton.setIcon(eliminar3);

        Image imgEliminar4= new ImageIcon("imagenes/papelera.jpeg").getImage();
        ImageIcon eliminar4=new ImageIcon(imgEliminar4.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        eliminarTecnicoButton2.setIcon(eliminar4);

        Image imagenTabbed= new ImageIcon("imagenes/imagen1.png").getImage();
        ImageIcon imag=new ImageIcon(imagenTabbed.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        tabbedPane.setIconAt(0,imag);

        Image imagenTabbed2= new ImageIcon("imagenes/imagenes2.jpeg").getImage();
        ImageIcon imag2=new ImageIcon(imagenTabbed2.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        tabbedPane.setIconAt(1,imag2);

        Image imagenTabbed3= new ImageIcon("imagenes/ima.jpeg").getImage();
        ImageIcon imag3=new ImageIcon(imagenTabbed3.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        tabbedPane.setIconAt(2,imag3);

        Image imagenTabbed4= new ImageIcon("imagenes/descarga (2).png").getImage();
        ImageIcon imag4=new ImageIcon(imagenTabbed4.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        tabbedPane.setIconAt(3,imag4);

        Image imgEliminar5= new ImageIcon("imagenes/papelera.jpeg").getImage();
        ImageIcon eliminar5=new ImageIcon(imgEliminar5.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        eliminarButton.setIcon(eliminar5);

        Image imgEliminar6= new ImageIcon("imagenes/papelera.jpeg").getImage();
        ImageIcon eliminar6=new ImageIcon(imgEliminar6.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        eliminarTipoDePersonajeButton.setIcon(eliminar6);

        Image imgEliminar7= new ImageIcon("imagenes/papelera.jpeg").getImage();
        ImageIcon eliminar7=new ImageIcon(imgEliminar5.getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        eliminarTipoDeTécnicoButton.setIcon(eliminar7);


    }


    /**
     * Setea una barra de menus
     */

    private void setMenu() {

        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");
        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame1.setJMenuBar(barra);
    }

public void fichero(){


    File fichero=new File("fichero.txt");
    Scanner lector = null;
    try {
        lector = new Scanner(fichero);

        while(lector.hasNextLine()){
            comboBoxPeliculaGénero.addItem(lector.nextLine());
        }
        lector.close();

    } catch (FileNotFoundException ex) {
        ex.printStackTrace();
    }


}


        public void ficheroActor(){


        File fichero=new File("ficheroActor.txt");
        Scanner lector = null;
        try {
            lector = new Scanner(fichero);

            while(lector.hasNextLine()){
                comboBoxTipoPersonaje.addItem(lector.nextLine());
            }
            lector.close();

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }


    }
    public void ficheroTecnico(){


        File fichero=new File("ficheroTecnico.txt");
        Scanner lector = null;
        try {
            lector = new Scanner(fichero);

            while(lector.hasNextLine()){
                comboBoxTecnico.addItem(lector.nextLine());
            }
            lector.close();

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }


    }
}




