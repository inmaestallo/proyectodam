package gui;


import enums.TipoUsuario;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */

public class InicioSesion extends JFrame {
    private JPanel panel2;
    JFrame frame;
    JTextField txtUsuario;
    JPasswordField passwordField1;
    JComboBox comboBoxTipoUsuario;
    JButton cancelarButton;
    JButton aceptarButton;
     JLabel jlabel1;
    Administrador administrador;
    Vista vista;
    Genero genero;

    /**
     * Constructor sin parámetros
     */
    public InicioSesion() {

        frame = new JFrame("Iniciar sesión");
        frame.setContentPane(panel2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(new Dimension(268,220));
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);


        Image img= new ImageIcon("imagenes/descarga.png").getImage();
        ImageIcon img2=new ImageIcon(img.getScaledInstance(258, 50, Image.SCALE_SMOOTH));

        jlabel1.setIcon(img2);


     //   jlabel1.setIcon(new ImageIcon("imagenes/descarga (1).png"));


        setEnumComboBox();

        aceptarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                onOK();


            }
        });
        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();

            }
        });
        comboBoxTipoUsuario.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);

            }
        });
    }

    /**
     * Método para salir de la aplicación
     */
    private void onCancel() {

        System.exit(0);
    }

    /**
     * Método que llama a otro método
     */
    private void onOK() {

        ingresar();


    }

    /**
     * Método para ingresar un usuario
     */

    public void ingresar() {

        String usuario = txtUsuario.getText();
        String contrasena = String.valueOf(passwordField1.getPassword());
        String tipoUsuario = String.valueOf(comboBoxTipoUsuario.getSelectedItem());



        if (usuario.equalsIgnoreCase("Admin") && contrasena.equalsIgnoreCase(String.valueOf(1111)) && tipoUsuario.contains("Administrador")) {

          administrador=new Administrador();







        }
       else if (usuario.equalsIgnoreCase("Usuario") && contrasena.equalsIgnoreCase(String.valueOf(2222)) && tipoUsuario.contains("Usuario")) {





                Modelo modelo = new Modelo();
                vista = new Vista();
                vista.panel1.setVisible(true);
                Controlador controlador = new Controlador(modelo, vista);
















        }
        else if (usuario.equalsIgnoreCase("Consultor") && contrasena.equalsIgnoreCase(String.valueOf(3333)) && tipoUsuario.contains("Consultor")) {

            Informes informes= new Informes();

        }


    }

    /**
     * Método para inicializar el combobox
     */
    private void setEnumComboBox() {

        for (TipoUsuario constant : TipoUsuario.values()) {
            comboBoxTipoUsuario.addItem(constant.getValor());
        }
        comboBoxTipoUsuario.setSelectedIndex(-1);

    }
}
