package gui;


import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener,KeyListener {

    private Modelo modelo;
    private Vista vista;
    private InicioSesion optionDialog;
    Connection conexion;
    boolean refrescar;


    /**
     * Constructor al que le paso dos parámetros
     * @param modelo
     * @param vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        this.conexion =modelo.conectar();
        addActionListeners(this);
        addWindowListeners(this);
        anadirKeyListener(this);
        refrescarTodo();

    }

    /**
     * Método en el que se activan los textos para buscar
     * @param listener
     */
    private void anadirKeyListener(KeyListener listener){

        vista.txtBuscarProductora.addKeyListener(listener);
        vista.txtBuscarPelicula.addKeyListener(listener);
        vista.txtBuscarActor.addKeyListener(listener);
        vista.txtBuscarTecnico.addKeyListener(listener);
    }

    /**
     * Método que refresca todos los campos
     */
    private void refrescarTodo() {
        refrescarProductora();
        refrescarPelicula();
        refrescarActor();
        refrescarTecnico();
        refrescar = false;
    }


    /**
     * Método que refresca la productora
     */

    public void refrescarProductora()  {

        try {
            vista.tableProductora.setModel(construirTableModelProductora(modelo.consultarProductora()));
            // vista.comboBoxPeliculaProductora.removeAllItems();
            for (int i = 0; i < vista.dtmProductora.getRowCount(); i++) {
                vista.comboBoxPeliculaProductora.addItem(vista.dtmProductora.getValueAt(i, 0) + " - " +
                        vista.dtmProductora.getValueAt(i, 2) + ", " + vista.dtmProductora.getValueAt(i, 1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Método que construye la tabla de productora
     * @param rs
     * @return vista.dtmProductora;
     * @throws SQLException
     */
    private TableModel construirTableModelProductora(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmProductora.setDataVector(data, columnNames);

        return vista.dtmProductora;
    }

    /**
     * Método que refresca una película
     */
    private void refrescarPelicula() {
        try {
            vista.tablePelicula.setModel(construirTableModelPelicula(modelo.consultarPelicula()));
            vista.comboBox1PeliculaActor.removeAllItems();
            for (int i = 0; i < vista.dtmPelicula.getRowCount(); i++) {
                vista.comboBox1PeliculaActor.addItem(vista.dtmPelicula.getValueAt(i, 0) + " - " +
                        vista.dtmPelicula.getValueAt(i, 2) + ", " + vista.dtmPelicula.getValueAt(i, 1));
            }
            vista.comboBoxTecnicoPelicula.removeAllItems();
            for (int i = 0; i < vista.dtmPelicula.getRowCount(); i++) {
                vista.comboBoxTecnicoPelicula.addItem(vista.dtmPelicula.getValueAt(i, 0) + " - " +
                        vista.dtmPelicula.getValueAt(i, 2) + ", " + vista.dtmPelicula.getValueAt(i, 1));
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }



    }

    /**
     * Método que construye a tabla película
     * @param rs
     * @return vista.dtmPelicula;
     * @throws SQLException
     */
    private TableModel construirTableModelPelicula(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmPelicula.setDataVector(data, columnNames);

        return vista.dtmPelicula;
    }

    /**
     * Método que refresca el actor
     */
    private void refrescarActor() {

        try {
            vista.tableActor.setModel(construirTableModeloActor(modelo.consultarActor()));



        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Método que construye la tabla de actor
     * @param rs
     * @return vista.dtmActor;
     * @throws SQLException
     */
    private TableModel construirTableModeloActor(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmActor.setDataVector(data, columnNames);

        return vista.dtmActor;
    }

    /**
     * Método que refresca el técnico
     */
    private void refrescarTecnico() {


        try {
            vista.tableTecnico.setModel(construirTableModeloTecnico(modelo.consultarTecnico()));
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    /**
     * Método que construye la tabla de técnico
     * @param rs
     * @return vista.dtmTecnico;
     * @throws SQLException
     */

    private TableModel construirTableModeloTecnico(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmTecnico.setDataVector(data, columnNames);

        return vista.dtmTecnico;
    }

    /**
     * Método en el que se crea el vector
     * @param rs
     * @param columnCount
     * @param data
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {

        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Método que activa el addWindowsListener
     * @param listener
     */
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Método que activa los botones
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.salirItem.addActionListener(listener);
        vista.conexionItem.addActionListener(listener);
        vista.añadirProductoraButton.addActionListener(listener);
        vista.eliminarProductoraButton.addActionListener(listener);
        vista.añadirPeliculaButton1.addActionListener(listener);
        vista.eliminarPeliculaButton1.addActionListener(listener);
        vista.añadirActorButton.addActionListener(listener);
        vista.eliminarActorButton.addActionListener(listener);
        vista.buttonAnadirTecnico.addActionListener(listener);
        vista.eliminarTecnicoButton2.addActionListener(listener);

    }

    /**
     * Método que borra los campos de la productora
     */
    private void borrarCamposProductora() {
        vista.txtNombreProductora.setText("");
        vista.txtFundador.setText("");
        vista.fechaProductora.setText("");
        vista.txtCiudad.setText("");
        vista.txtpaisProductora.setText("");
        vista.txtCantidadPersonasPR.setText("");
        vista.txtCantidadDinero.setText("");

    }

    /**
     * Método que comprueba si los campos de productora están vacios
     * @return vista.txtNombreProductora.getText().isEmpty() ||
     *                 vista.txtFundador.getText().isEmpty()||
     *                 vista.fechaProductora.getText().isEmpty() ||
     *                 vista.txtCiudad.getText().isEmpty() ||
     *                 vista.txtpaisProductora.getText().isEmpty() ||
     *                 vista.txtCantidadPersonasPR.getText().isEmpty() ||
     *                 vista.txtCantidadDinero.getText().isEmpty();
     */
    private boolean comprobarProductoraVacio() {
        return  vista.txtNombreProductora.getText().isEmpty() ||
                vista.txtFundador.getText().isEmpty()||
                vista.fechaProductora.getText().isEmpty() ||
                vista.txtCiudad.getText().isEmpty() ||
                vista.txtpaisProductora.getText().isEmpty() ||
                vista.txtCantidadPersonasPR.getText().isEmpty() ||
                vista.txtCantidadDinero.getText().isEmpty();

    }

    /**
     * Método que borra los campos de película
     */
    private void borrarCamposPelicula() {
        vista.txtNombrePelicula.setText("");
        vista.txtDirector.setText("");
        vista.fechaInicioPelicula.setText("");
        vista.fechaFinPelicula.setText("");
        vista.comboBoxPeliculaProductora.setSelectedIndex(-1);
        vista.comboBoxPeliculaGénero.setSelectedIndex(-1);
        vista.txtLugarGraba.setText("");

    }

    /**
     * Método que comprueba si la película esta vacia
     * @return vista.txtNombrePelicula.getText().isEmpty() ||
     *                 vista.txtDirector.getText().isEmpty() ||
     *                 vista.fechaInicioPelicula.getText().isEmpty() ||
     *                 vista.fechaFinPelicula.getText().isEmpty() ||
     *                 vista.comboBoxPeliculaProductora.getSelectedIndex() == -1 ||
     *                 vista.comboBoxPeliculaGénero.getSelectedIndex() == -1 ||
     *                 vista.txtLugarGraba.getText().isEmpty() ;
     */
    private boolean comprobarPeliculaVacio() {

        return  vista.txtNombrePelicula.getText().isEmpty() ||
                vista.txtDirector.getText().isEmpty() ||
                vista.fechaInicioPelicula.getText().isEmpty() ||
                vista.fechaFinPelicula.getText().isEmpty() ||
                vista.comboBoxPeliculaProductora.getSelectedIndex() == -1 ||
                vista.comboBoxPeliculaGénero.getSelectedIndex() == -1 ||
                vista.txtLugarGraba.getText().isEmpty() ;

    }

    /**
     * Método que borra los campos de actor
     */
    private void borrarCamposActor() {
        vista.txtNombreActor.setText("");
        vista.txtApellidoActor.setText("");
        vista.txtEdadActor.setText("");
        vista.comboBoxTipoPersonaje.setSelectedIndex(-1);
        vista.txtNacionalidad.setText("");
        vista.txtPremios.setText("");
        vista.txtExperiencia.setText("");
        vista.comboBox1PeliculaActor.setSelectedIndex(-1); ;

    }

    /**
     * Método que comprueba si el actor esta vacio
     * @return vista.txtNombreActor.getText().isEmpty() ||
     *                 vista.txtApellidoActor.getText().isEmpty() ||
     *                 vista.txtEdadActor.getText().isEmpty() ||
     *                 vista.comboBoxTipoPersonaje.getSelectedIndex() == -1 ||
     *                 vista.txtNacionalidad.getText().isEmpty() ||
     *                 vista.txtPremios.getText().isEmpty() ||
     *                 vista.txtExperiencia.getText().isEmpty()||
     *                 vista.comboBox1PeliculaActor.getSelectedIndex() == (-1) ;
     */
    private boolean comprobarActorVacio() {

        return  vista.txtNombreActor.getText().isEmpty() ||
                vista.txtApellidoActor.getText().isEmpty() ||
                vista.txtEdadActor.getText().isEmpty() ||
                vista.comboBoxTipoPersonaje.getSelectedIndex() == -1 ||
                vista.txtNacionalidad.getText().isEmpty() ||
                vista.txtPremios.getText().isEmpty() ||
                vista.txtExperiencia.getText().isEmpty()||
                vista.comboBox1PeliculaActor.getSelectedIndex() == (-1) ;

    }

    /**
     * Método que borra los campos de tecnico
     */
    private void borrarCamposTecnico() {

        vista.txtNombeTecnico.setText("");
        vista.txtApellido.setText("");
        vista.txtEdadTecnico.setText("");
        vista.comboBoxTecnico.setSelectedIndex(-1);
        vista.textFieldAniosTecnico.setText("");
        vista.FechainicioTecnico.setText("");
        vista.fechaFinalTecnico.setText("");
        vista.comboBoxTecnicoPelicula.setSelectedIndex(-1);

    }

    /**
     * Método que comprueba si el técnico esta vacío
     * @return vista.txtNombeTecnico.getText().isEmpty() ||
     *                 vista.txtApellido.getText().isEmpty() ||
     *                 vista.txtEdadTecnico.getText().isEmpty() ||
     *                 vista.comboBoxTecnico.getSelectedIndex() == -1 ||
     *                 vista.textFieldAniosTecnico.getText().isEmpty() ||
     *                 vista.FechainicioTecnico.getText().isEmpty() ||
     *                 vista.fechaFinalTecnico.getText().isEmpty()||
     *                 vista.comboBoxTecnicoPelicula.getSelectedIndex() == (-1);
     */
    private boolean comprobarTecnicoVacio() {

        return vista.txtNombeTecnico.getText().isEmpty() ||
                vista.txtApellido.getText().isEmpty() ||
                vista.txtEdadTecnico.getText().isEmpty() ||
                vista.comboBoxTecnico.getSelectedIndex() == -1 ||
                vista.textFieldAniosTecnico.getText().isEmpty() ||
                vista.FechainicioTecnico.getText().isEmpty() ||
                vista.fechaFinalTecnico.getText().isEmpty()||
                vista.comboBoxTecnicoPelicula.getSelectedIndex() == (-1);
    }

    /**
     * Método que acciona los botones
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();


        switch (command) {

            case "Desconectar":
                modelo.desconectar();
                break;

            case "Salir":
                System.exit(0);
                break;

            case "Añadir Productora": {
                try {
                    if (comprobarProductoraVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableProductora.clearSelection();
                    } else if (modelo.productoraYaExiste(vista.txtNombreProductora.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce otro");
                        vista.tableProductora.clearSelection();
                    } else {
                        modelo.insertarProductora(
                                vista.txtNombreProductora.getText(),
                                vista.txtFundador.getText(),
                                vista.fechaProductora.getDate(),
                                vista.txtCiudad.getText(),
                                vista.txtpaisProductora.getText(),
                                Integer.parseInt(vista.txtCantidadPersonasPR.getText()),
                                        Integer.parseInt( vista.txtCantidadDinero.getText()));


                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableProductora.clearSelection();
                }
                borrarCamposProductora();
                refrescarProductora();

            }
            break;

            case "Eliminar Productora":
                modelo.borrarProductora(Integer.parseInt((String)vista.tableProductora.getValueAt(vista.tableProductora.getSelectedRow(), 0)));
                borrarCamposProductora();
                refrescarProductora();
                break;

            case "Añadir Pelicula": {
                try {
                    if (comprobarPeliculaVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablePelicula.clearSelection();
                    } else if (modelo.peliculaYaExiste(vista.txtNombrePelicula.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce otro");
                        vista.tablePelicula.clearSelection();

                    } else {
                        modelo.insertarPelicula(
                                vista.txtNombrePelicula.getText(),
                                vista.txtDirector.getText(),
                                vista.fechaInicioPelicula.getDate(),
                                vista.fechaFinPelicula.getDate(),
                                String.valueOf(vista.comboBoxPeliculaProductora.getSelectedItem()),
                                String.valueOf(vista.comboBoxPeliculaGénero.getSelectedItem()),
                                vista.txtLugarGraba.getText());


                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablePelicula.clearSelection();
                }
                borrarCamposPelicula();
                refrescarPelicula();

            }
            break;

            case "Eliminar Pelicula":
                modelo.borrarPelicula(Integer.parseInt((String)vista.tablePelicula.getValueAt(vista.tablePelicula.getSelectedRow(), 0)));
                borrarCamposPelicula();
                refrescarPelicula();
                break;

            case "Añadir Actor": {
                try {
                    if (comprobarActorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableActor.clearSelection();
                    } else if (modelo.actorYaExiste(vista.txtNombreActor.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce otro");
                        vista.tableActor.clearSelection();


                    } else {
                        modelo.insertarActor(
                                 vista.txtNombreActor.getText(),
                                vista.txtApellidoActor.getText(),
                                Integer.parseInt(vista.txtEdadActor.getText()),
                                String.valueOf(vista.comboBoxTipoPersonaje.getSelectedItem()),
                                vista.txtNacionalidad.getText(),
                                Integer.parseInt(vista.txtPremios.getText()),
                                Integer.parseInt(vista.txtExperiencia.getText()),
                                String.valueOf(vista.comboBox1PeliculaActor.getSelectedItem()));
                        refrescarActor();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableActor.clearSelection();
                }
                borrarCamposActor();

            }
            break;

            case "Eliminar Actor":
                modelo.borrarActor(Integer.parseInt((String)vista.tableActor.getValueAt(vista.tableActor.getSelectedRow(), 0)));
                borrarCamposActor();
                refrescarActor();
                break;

            case "Añadir Tecnico": {
                try {
                    if (comprobarTecnicoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableTecnico.clearSelection();
                    } else if (modelo.tecnicoYaExiste(vista.txtNombeTecnico.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce otro");
                        vista.tableTecnico.clearSelection();

                    } else {
                        modelo.insertarTecnico(
                                vista.txtNombeTecnico.getText(),
                                vista.txtApellido.getText(),
                                Integer.parseInt(vista.txtEdadTecnico.getText()),
                                String.valueOf(vista.comboBoxTecnico.getSelectedItem()),
                                Integer.parseInt(vista.textFieldAniosTecnico.getText()),
                                vista.FechainicioTecnico.getDate(),
                                vista.fechaFinalTecnico.getDate(),
                                String.valueOf(vista.comboBoxTecnicoPelicula.getSelectedItem()));

                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableTecnico.clearSelection();
                }
                borrarCamposTecnico();
                refrescarTecnico();
            }
            break;

            case "Eliminar Tecnico":
                modelo.borrarTecnico(Integer.parseInt((String)vista.tableTecnico.getValueAt(vista.tableTecnico.getSelectedRow(), 0)));
                borrarCamposTecnico();
                refrescarTecnico();
                break;

        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Método que añade cada uno de los campos a la tabla
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tableTecnico.getSelectionModel())) {
                int row = vista.tableTecnico.getSelectedRow();
                vista.txtNombeTecnico.setText(String.valueOf(vista.tableTecnico.getValueAt(row, 1)));
                vista.txtApellido.setText(String.valueOf(vista.tableTecnico.getValueAt(row, 2)));
                vista.txtEdadTecnico.setText(String.valueOf(vista.tableTecnico.getValueAt(row, 3)));
                vista.comboBoxTecnico.setSelectedItem(String.valueOf(vista.tableTecnico.getValueAt(row, 4)));
                vista.textFieldAniosTecnico.setText(String.valueOf(vista.tableTecnico.getValueAt(row, 5)));
                vista.FechainicioTecnico.setDate((Date.valueOf(String.valueOf(vista.tableTecnico.getValueAt(row, 6)))).toLocalDate());
                vista.fechaFinalTecnico.setDate((Date.valueOf(String.valueOf(vista.tableTecnico.getValueAt(row, 7)))).toLocalDate());
                vista.comboBoxTecnicoPelicula.setSelectedItem(String.valueOf(vista.tableTecnico.getValueAt(row, 8)));

            }

          else if (e.getSource().equals(vista.tableActor.getSelectionModel())) {
                int row = vista.tableActor.getSelectedRow();
                vista.txtNombreActor.setText(String.valueOf(vista.tableActor.getValueAt(row, 1)));
                vista.txtApellidoActor.setText(String.valueOf(vista.tableActor.getValueAt(row, 2)));
                vista.txtEdadActor.setText(String.valueOf(vista.tableActor.getValueAt(row, 3)));
                vista.comboBoxTipoPersonaje.setSelectedItem(String.valueOf(vista.tableActor.getValueAt(row, 4)));
                vista.txtNacionalidad.setText(String.valueOf(vista.tableActor.getValueAt(row, 5)));
                vista.txtPremios.setText(String.valueOf(vista.tableActor.getValueAt(row, 6)));
                vista.txtExperiencia.setText(String.valueOf(vista.tableActor.getValueAt(row, 7)));
                vista.comboBox1PeliculaActor.setSelectedItem(String.valueOf(vista.tableTecnico.getValueAt(row, 8)));
            }

            else if (e.getSource().equals(vista.tablePelicula.getSelectionModel())) {
                int row = vista.tablePelicula.getSelectedRow();
                vista.txtNombrePelicula.setText(String.valueOf(vista.tablePelicula.getValueAt(row, 1)));
                vista.txtDirector.setText(String.valueOf(vista.tablePelicula.getValueAt(row, 2)));
                vista.fechaInicioPelicula.setDate((Date.valueOf(String.valueOf(vista.tablePelicula.getValueAt(row, 3)))).toLocalDate());
                vista.fechaFinPelicula.setDate((Date.valueOf(String.valueOf(vista.tablePelicula.getValueAt(row, 4)))).toLocalDate());
                vista.comboBoxPeliculaProductora.setSelectedItem(String.valueOf(vista.tablePelicula.getValueAt(row, 5)));
                vista.comboBoxPeliculaGénero.setSelectedItem(String.valueOf(vista.tablePelicula.getValueAt(row, 6)));
                vista.txtLugarGraba.setText(String.valueOf(vista.tablePelicula.getValueAt(row, 7)));
            }
            else if (e.getSource().equals(vista.tableProductora.getSelectionModel())) {
                int row = vista.tableProductora.getSelectedRow();
                vista.txtNombreProductora.setText(String.valueOf(vista.tableProductora.getValueAt(row, 1)));
                vista.txtFundador.setText(String.valueOf(vista.tableProductora.getValueAt(row, 2)));
                vista.fechaProductora.setDate((Date.valueOf(String.valueOf(vista.tableProductora.getValueAt(row, 3)))).toLocalDate());
                vista.txtCiudad.setText(String.valueOf(vista.tableProductora.getValueAt(row, 4)));
                vista.txtpaisProductora.setText(String.valueOf(vista.tableProductora.getValueAt(row, 5)));
                vista.txtCantidadPersonasPR.setText(String.valueOf(vista.tableProductora.getValueAt(row, 6)));
                vista.txtCantidadDinero.setText(String.valueOf(vista.tableProductora.getValueAt(row, 7)));


            }else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.tableProductora.getSelectionModel())) {
                    borrarCamposProductora();
                } else if (e.getSource().equals(vista.tablePelicula.getSelectionModel())) {
                    borrarCamposPelicula();
                } else if (e.getSource().equals(vista.tableActor.getSelectionModel())) {
                    borrarCamposActor();
                }
                else if (e.getSource().equals(vista.tableTecnico.getSelectionModel())) {
                    borrarCamposTecnico();
                }
            }

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Método que al escribir el texto te busca el campo que quieres
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        try {
            cargarFilasProductora(modelo.buscarProductora(vista.txtBuscarProductora.getText()));
            cargarFilasPelicula(modelo.buscarPelicula(vista.txtBuscarPelicula.getText()));
            cargarFilasActor(modelo.buscarActor(vista.txtBuscarActor.getText()));
            cargarFilasTecnico(modelo.buscarTecnico(vista.txtBuscarTecnico.getText()));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        ;

    }

    /**
     * Método que te carga las filas de la productora
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasProductora(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[7];
        vista.dtmProductora.setRowCount(0);

        while(resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);



            vista.dtmProductora.addRow(fila);

        }

        if(resultSet.last()){
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }




    }

    /**
     * Método que te carga las filas de la película
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasPelicula(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[7];
        vista.dtmPelicula.setRowCount(0);

        while(resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);



            vista.dtmPelicula.addRow(fila);

        }

        if(resultSet.last()){
            vista.lblAccionPeli.setVisible(true);
            vista.lblAccionPeli.setText(resultSet.getRow()+" filas cargadas");
        }




    }

    /**
     * Método que te carga las filas del actor
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasActor(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[8];
        vista.dtmActor.setRowCount(0);

        while(resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);



            vista.dtmActor.addRow(fila);

        }

        if(resultSet.last()){
            vista.lblAccionActor.setVisible(true);
            vista.lblAccionActor.setText(resultSet.getRow()+" filas cargadas");
        }




    }

    /**
     * Método que te carga las filas del técnico
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasTecnico(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[8];
        vista.dtmTecnico.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);


            vista.dtmTecnico.addRow(fila);

        }

        if (resultSet.last()) {
            vista.lblAccionTecnico.setVisible(true);
            vista.lblAccionTecnico.setText(resultSet.getRow() + " filas cargadas");
        }
    }



    }


