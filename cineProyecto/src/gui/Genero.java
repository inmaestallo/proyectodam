package gui;

public class Genero {

    String nombreG;

    /**
     * Constructor vacio
     */
    public Genero() {
    }

    /**
     * Constructor al que le paso un parámetro
     * @param nombreG
     */
    public Genero(String nombreG) {
        this.nombreG = nombreG;
    }

    /**
     * Método que retorna el nombreG
     * @return nombreG
     */
    public String getNombreG() {
        return nombreG;
    }

    /**
     * Método al que se le pasa un parámetro
     * @param nombreG
     */
    public void setNombreG(String nombreG) {
        this.nombreG = nombreG;
    }

    /**
     * Mñetodo que visualiza el nombreG
     * @return
     */
    @Override
    public String toString() {
        return nombreG ;
    }
}
