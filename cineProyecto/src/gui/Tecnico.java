package gui;
/**
 * @author inma Estallo Puyuelo
 * @version 1.1
 * @since 1
 */
public class Tecnico {
    String nombreTecnnico;

    /**
     * Contructor al que se le pasa un parámetro
     * @param nombreTecnnico
     */
    public Tecnico(String nombreTecnnico) {
        this.nombreTecnnico = nombreTecnnico;
    }

    /**
     * Contructor vacío
     */
    public Tecnico() {
    }

    /**
     * Método getgetNombreTecnnico(), que devuelve el nombre del técnico
     * @return nombreTecnnico
     */
    public String getNombreTecnnico() {
        return nombreTecnnico;
    }

    /**
     * Mñetodo setNombreTecnnico(), al que se le pasa el parámetro nombreTecnnico
     * @param nombreTecnnico
     */
    public void setNombreTecnnico(String nombreTecnnico) {
        this.nombreTecnnico = nombreTecnnico;
    }

    /**
     * Métod que devuelve el nombre del técnico
     * @return nombreTecnnico
     */
    @Override
    public String toString() {
        return nombreTecnnico ;
    }
}
