CREATE DATABASE if not exists cine;
--
USE cine ;
 
--
CREATE TABLE IF NOT EXISTS productora (
idProductora int auto_increment primary key,
nombreProductora varchar(25) not null,
fundador varchar(25) not null,
fechanCreacionP date,
ciudad varchar(25) not null,
pais varchar(100) not null,
cantidadPersonas int,
cantidadDinero int
 );
 --

insert INTO `productora` (`idProductora`, `nombreProductora`, `fundador`, `fechanCreacionP`,`ciudad`,`pais`,`cantidadPersonas`,`cantidadDinero`) VALUES
(1, 'Warner Bros ','Hermanos Warner',' 1923-07-10', 'Nueva york', 'Estados Unidos',2.000,900.000),
(2, 'WaltDisney ','Walt Disney',' 1952-12-16' ,'Nueva york', 'Estados Unidos',2.000,700.000),
(3, 'Universal ','Carl Laemmie',' 1977-07-09' ,'Nueva york', 'Estados Unidos',2.000,725.000),
(4, 'Pixar ','Edwin Catmull',' 1974-02-10' ,'Nueva york', 'Estados Unidos',2.000,600.000),
(5, 'Dreamworks pictures ','Steven Spielberg',' 1994-12-10' ,'Nueva york', 'Estados Unidos',2.000,880.000),
(6, 'Atresmedia ','Juan',' 200-03-11' ,'Nueva york', 'Estados Unidos',1.000,120.000),
(7, 'Altafilms ','Antonio Martinez',' 2000-03-11' ,'Madrid', 'España',1.000,100.000),
(8, 'La magia ','Miguel García',' 1999-04-23' ,'Madrid', 'España',100,50.000),
(9, 'Zebra ','Tom Castro',' 1990-10-05' ,'Madrid', 'España',100,20.000);

 --
CREATE TABLE IF NOT EXISTS pelicula(
idPelicula int auto_increment primary key,
nombrePelicula varchar(25) not null,
directorPelicula varchar(25) not null,
fechanInicioGraba date,
fechanFinGraba date,
idProductora int,
genero varchar(25),
lugarGrabacion varchar(25) not null
);
--

insert INTO `pelicula` (idPelicula, nombrePelicula, directorPelicula,fechanInicioGraba,fechanFinGraba,idProductora,genero,lugarGrabacion) VALUES
(1, 'Los nuestros ',' Carlos' ,'2020-02-10','2020-10-10', 9 ,'Aventura','Italia'),
(2, 'Aladdin ',' Ron Clements' ,'1998-01-01','1999-12-30', 2 ,'Familiar','Estados Unidos'),
(3, 'El rel león ',' Rob Minkoff' ,'1992-02-10','1994-01-02', 2 ,'Familiar','Estados Unidos'),
(4, 'La bella y la bestia ','Kirk Wise','2020-02-10','2020-10-10', 2 ,'Familiar','Italia'),
(5, 'Ocho apellidos Vascos ','	Emilio Martínez-Lázaro' ,'2012-02-10','2013-10-10', 9 ,'Comedia','España'),
(6, 'Un día ',' Juan de los Palotes' ,'2020-02-10','2020-10-10', 9 ,'Oeste','España'),
(7, 'El hobbit ',' Peter Jackson','2010-02-10','2011-10-10', 1 ,'Fantasía','Nueva Zelanda'),
(8, 'Batman ',' Christopher Nolan' ,'2010-02-10','2011-11-10', 1 ,'Fantasía','Nueva York'),
(9, 'Lo que el viento ',' James' ,'1988-03-21','1989-04-30', 1 ,'Aventura','California'),
(10, 'La lista de Shindler ',' Steven Spielberg' ,'1991-03-21','1992-04-30', 5 ,'Aventura','Alemania'),
(11, 'Sherk ','  Andrew Adamson' ,'2002-03-21','2003-04-30', 5 ,'Familiar','Estados Unidos'),
(12, 'Trolls ','  Mike Mitchell' ,'2014-03-21','2015-04-30', 5 ,'Familiar','Estados Unidos'),
(13, 'Madagascar ',' Tom McGrath' ,'2003-03-21','2004-04-30', 5 ,'Familiar','Estados Unidos'),
(14, 'El discruso del rey ',' Malnolo' ,'2000-03-21','2002-04-30', 8 ,'Aventura','España'),
(15, 'El asesino ',' Juan Miedo' ,'2020-03-21','2020-04-30',8 ,'Policiaca','España'),
(16, 'Cazatesoros ',' Santiago' ,'2020-03-21','2020-04-30', 8 ,14,'España'),
(17, 'Indiama ',' Jones' ,'2012-06-02','2015-09-10', 8 ,'Terror','Londres'),
(18, ' Coronavirus letal',' Pedro Sanches' ,'2020-06-02','2020-09-10', 6 ,'Ficcion','España'),
(19, 'Superviviente ',' Carlos Castel' ,'2015-06-02','2016-09-10', 6 ,'Terror','Londres'),
(20, 'Encerrados ',' Pablo Iglesias' ,'2020-06-02','2020-09-10', 6 ,'Terror','Londres');




 --
CREATE TABLE IF NOT EXISTS actor(
idActor int auto_increment primary key,
nombreActor varchar(25) not null,
apellidoActor varchar(25) not null,
edadActor int,
tipoPersonaje varchar (50)not null,
nacionalidad varchar(25) not null,
premios int,
experienciaActor int,
idPelicula int
);


insert INTO `actor` (idActor, nombreActor, apellidoActor, edadActor,tipoPersonaje,nacionalidad,premios,experienciaActor,idPelicula) VALUES
(1, 'Manolo ',' Escudero' ,42,' Secundario', 'Española' ,2,20,9),
(2, 'Erinque ',' Cuartero' ,42,' Protagonista', 'Española' ,3,20,9),
(3, 'Leonardo ',' Dicaprio' ,45,' Secundario', 'Estadounidense' ,20,40,1),
(4, 'Brad ',' Pitt' ,56,' Protagonista', 'Estadounidense' ,40,50,1),
(5, 'Pepe ',' Navarro' ,35,' Secundario', 'Española' ,15,20,17),
(6, 'Angelina ',' Jolie' ,44,' Protagonista', 'Estadounidense' ,30,54,3),
(7, 'Penelope ',' cruz' ,45,' Secundario', 'Española' ,20,30,17),
(8, 'Santiago  ',' Segura' ,42,' Figuración', 'Española' ,10,20,17),
(9, 'Fernando ',' Ocaña' ,33,' Doble', 'Española' ,2,20,8),
(10, 'Tom ',' Hanks' ,63,' Cameo', 'Estadounidense' ,50,40,3),
(11, 'Erik ',' Dolores' ,20,' Secundario', 'Española' ,0,9,10),
(12, 'María',' Frago' ,34,' Especialista', 'Española' ,6,2,10),
(13, 'Juan ',' Porta' ,38,' Doble', 'Española' ,10,3,10),
(14, 'Enrique ',' Ponce' ,49,' Secundario', 'Española' ,8,12,4),
(15, 'Lucas ',' Romero' ,27,' Secundario', 'Española' ,2,7,3);

 --
CREATE TABLE IF NOT EXISTS tecnico(
idTecnico int auto_increment primary key,
nombreTecnico varchar(25) not null,
apellidoTecnico varchar(25) not null,
edadTecnico int,
tipoTecnico varchar (50)not null,
idPelicula int,
aniosTrabajados int,
fechaInicioT date,
fechaFinalT date
);
--

insert INTO `tecnico` (idTecnico, nombreTecnico, apellidoTecnico, edadTecnico,tipoTecnico,idPelicula,aniosTrabajados,fechaInicioT,fechaFinalT) VALUES
(1, 'Juan ',' Giménez' ,42,' Operador de cámara',1, 20 ,'2020-02-10','2020-10-10'),
(2, 'Miguel ',' Castro' ,35,' Iluminación', 1,10 ,'2020-02-10','2020-10-10'),
(3, 'Ana ',' Gasol' ,28,' Efectos especiales',9, 6 ,'2020-02-10','2020-10-10'),
(4, 'Carlos ',' Gil' ,30,' Operador de cámara',9, 9 ,'2020-02-10','2020-10-10'),
(5, 'Merche ',' Martinez' ,27,' Técnico de sonido',9, 6 ,'2020-02-10','2020-10-10'),
(6, 'Sergio ',' Giménez' ,50,' Operador de cámara',10, 20 ,'2020-02-10','2020-10-10'),
(7, 'Elena ',' Carballo' ,25,' Maquillaje', 10,5 ,'2020-02-10','2020-10-10'),
(8, 'Santiago ',' Cañizares' ,33,' Claquetista',10,10 ,'2020-02-10','2020-10-10'),
(9, 'Luis ',' Sotomonte' ,55,' Decorador', 3,9 ,'2020-02-10','2020-10-10'),
(10, 'Rubén ',' Agapito' ,39,' Peluquero', 3,10 ,'2020-02-10','2020-10-10'),
(11, 'Antonio ',' Segura' ,25,' Microfonista', 3,4 ,'2020-02-10','2020-10-10'),
(12, 'Adrian ',' Solis' ,20,' Operador de cámara',17, 3 ,'2020-02-10','2020-10-10'),
(13, 'Jake ',' Clintom' ,29,' Maquillaje',17, 6 ,'2020-02-10','2020-10-10'),
(14, 'Francisco ',' Castel' ,53,' Maquillaje', 17,30 ,'2020-02-10','2020-10-10'),
(15, 'Adriana ',' Alonso' ,23,' Efectos especiales',17, 5 ,'2020-02-10','2020-10-10');

--

alter table  pelicula
add foreign key (idProductora) references productora(idProductora);
--
alter table actor
add foreign key (idPelicula) references pelicula(idPelicula);
--
alter table tecnico
add foreign key (idPelicula) references pelicula(idPelicula);
--
delimiter ||
create function existeProductora(f_nombre varchar(25))
returns bit
begin
	declare i int;
	set i=0;
	while (i<(select max(idProductora) from productora)) do
	if ((select nombreProductora from productora
		 where idProductora=(i+1)) like f_nombre) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end; ||
delimiter ;
--
delimiter ||
create function existePelicula(f_nombre varchar(25))
returns bit
begin
	declare i int;
	set i=0;
	while (i<(select max(idPelicula) from pelicula)) do
	if ((select nombrePelicula  from pelicula
		 where idPelicula=(i+1)) like f_nombre) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeActor(f_nombre varchar(25))
returns bit
begin
	declare i int;
	set i=0;
	while (i<(select max(idActor) from actor)) do
	if ((select nombreActor  from actor
		 where idActor=(i+1)) like f_nombre) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeTecnico(f_nombre varchar(9))
returns bit
begin
	declare i int;
	set i=0;
	while (i<(select max(idTecnico) from tecnico)) do
	if ((select nombreTecnico  from tecnico
		 where idTecnico=(i+1)) like f_nombre) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end; ||
delimiter ;
--

